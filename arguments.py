import sys

# argv: argument vector, 命令行参数向量（内容）
# argc：argument count，命令行参数个数，C语言中有这个属性 python 并没有提供，python通过len(sys.argv)获得

# it's easy to print this list of course:
print(sys.argv)

# or it can be iterated via a for loop:

for i in range(len(sys.argv)):
    if i == 0:
        print("Function name: %s" % sys.argv[0])
    else:
        print("%d. argument: %s" % (i, sys.argv[i]))
