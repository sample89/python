import sys

x = 42

print(x)

def my_display(x):
    print("out: ")
    print(x)

sys.displayhook = my_display

print(x)
